import domain.*;
import service.PartService;
import service.RepairService;
import service.UserService;
import service.VehicleService;
import util.RepairStatus;

public class Main {
    public static void main(String[] args) {

        UserService userService = UserService.getInstance();
        VehicleService vehicleService = VehicleService.getInstance();
        RepairService repairService = RepairService.getInstance();
        PartService partService = PartService.getInstance();

        User user = new User(1L, "ab", "ab", "ab", "ab", "ab", 123, "ab");
        boolean userCreated = userService.create(user);
        if(userCreated) {
            System.out.println("User successfully created");
        }
        else {
            System.out.println("User not created");
        }
        Vehicle vehicle = new Vehicle(11L, "aaa-222", "BA", "ac", "red",
                34.4, 1L, 2019, 2, 25);
        boolean vehicleCreated = vehicleService.create(vehicle);
        if(vehicleCreated) {
            System.out.println("Vehicle created");
        }
        else {
            System.out.println("Vehicle not created");
        }
        Repair repair = new Repair(21L, RepairStatus.PENDING, 11L, 33.3, 2019, 2, 25);
        repair.setStatus(RepairStatus.COMPLETED);
        boolean repairCreated = repairService.createRepair(repair);
        if (repairCreated) {
            System.out.println("Repair successfully created");
        }
        else {
            System.out.println("Repair not created\nInvalid vehicle ID");
        }
        Part part = new Part(31L, "ab", "aa", 22.2, 21L);
        boolean partCreated = partService.createPart(part);
        if(partCreated) {
            System.out.println("Part created");
        } else {
            System.out.println("part not created");
        }



        System.out.println(partService.findAllParts());
        System.out.println(userService.findAllRepairsOfAllVehiclesByUser(1L));
        System.out.println(userService.findAllVehiclesByUeer(1L));
        User user2 = new User(1L, "ab", "ab", "acb", "acb", "ab", 123, "ab");
        userCreated = userService.create(user2);
        if(userCreated) {
            System.out.println("User created");
        }
        else {
            System.out.println("User not created\nInvalid ID");
        }


        User user3 = new User(3L, "ab", "ab", "acb", "acb", "ab", 123, "ab");
        userService.create(user3);
        System.out.println(userService.findAll());
        userService.delete(3L);
        System.out.println(userService.findAll());
        System.out.println(userService.findByEmail("a"));

        Repair repair1 = new Repair(22L, RepairStatus.COMPLETED, 11L, 22.2, 2018, 2, 25);
        if(repairService.createRepair(repair1)) {
            System.out.println("Repair successfully created");
        }
        else {
            System.out.println("Repair not created\nInvalid vehicle ID");
        }
        Part part2 = new Part(32L, "ac", "bb", 11.1, 22L);
        partCreated = partService.createPart(part2);
        if(partCreated) {
            System.out.println("Part created");
        } else {
            System.out.println("part not created");
        }
        Part part3 = new Part(33L, "qw", "ew", 99.8, 22L);
        partCreated = partService.createPart(part3);
        if(partCreated) {
            System.out.println("Part created");
        } else {
            System.out.println("part not created");
        }

        System.out.println(partService.findAllParts());
        System.out.println(userService.findAllRepairsOfAllVehiclesByUser(1L));
        System.out.println(userService.findAllVehiclesByUeer(1L));
        Vehicle vehicle4 = new Vehicle(14L, "xxx-909", "bv", "po", "RED", 54.3, 1L,
                2019, 5, 13);
        vehicleService.create(vehicle4);
        System.out.println(userService.findAllVehiclesByUeer(1L));
        Vehicle vehicle5 = new Vehicle(14L, "zzz-099", "BV", "PO", "red", 45.3,
                1L, 1920, 5, 13);
        vehicleService.updateById(vehicle5);
        System.out.println(userService.findAllVehiclesByUeer(1L));

        Vehicle vehicle1 = new Vehicle(13L, "aaa-333", "a", "b", "g", 44.4, 55L,
                2008, 4, 5);
        vehicleCreated = vehicleService.create(vehicle1);
        if(vehicleCreated==false) {
            System.out.println("Invalid userID for vehicle");
        }
    }
}
