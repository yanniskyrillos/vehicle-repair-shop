package persistence;

import domain.Part;
import domain.Repair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RepairDAO {

    private List<Repair> REPAIR_LIST = new ArrayList<>();

    private static RepairDAO repairDAO;
    private RepairDAO() {}
    public static RepairDAO getInstance() {
        if (repairDAO == null) {
            repairDAO = new RepairDAO();
        }
        return repairDAO;
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        List<Repair> allRepairsByVehicle = new ArrayList<>();
        for(Repair repair: REPAIR_LIST) {
            if(repair.getVehicleId().equals(vehicleId)) {
                allRepairsByVehicle.add(repair);
            }
        }
        return allRepairsByVehicle;
    }

    public void addRepairToList(Repair repair) {
        REPAIR_LIST.add(repair);
    }

    public List<Repair> findAllRepairs() {
        return REPAIR_LIST;
    }

    public Repair findRepairById(Long id) {
        for(Repair repair: REPAIR_LIST) {
            if (repair.getId().equals(id)) {
                return repair;
            }
        }
        return null;
    }

    public void updateRepairById(Long id, Repair repairUpdated) {
        Iterator<Repair> repairIterator = REPAIR_LIST.iterator();
        while (repairIterator.hasNext()) {
            Repair repair = repairIterator.next();
            if (repair.getId().equals(id)) {
                int index = REPAIR_LIST.indexOf(repair);
                REPAIR_LIST.set(index, repairUpdated);
                return;
            }
        }
    }

    public void deleteRepairById(Long id) {
        Iterator<Repair> repairIterator = REPAIR_LIST.iterator();
        while (repairIterator.hasNext()) {
            Repair repair = repairIterator.next();
            if(repair.getId().equals(id)) {
                repairIterator.remove();
                return;
            }
        }
    }

    public double calculateCostOfRepairAndParts(Long id) {
        double cost = 0.0;
        for(Repair repair: REPAIR_LIST) {
            if (repair.getId().equals(id)) {
                cost = cost + repair.getCost();
                for (int i = 0; i< repair.getNumberOfParts(); i++) {
                    cost = cost + repair.getPart(i).getCost();
                }
            }
        }
        return cost;
    }

    public void removePart(Part partToRemove) {
        Iterator<Repair> repairIterator = REPAIR_LIST.iterator();
        while (repairIterator.hasNext()) {
            Repair repair = repairIterator.next();
            repair.removePart(partToRemove);
            return;
        }

    }

    public void addPartToRepair(Part part) {
        Iterator<Repair> repairIterator = REPAIR_LIST.iterator();
        while (repairIterator.hasNext()) {
            Repair repair = repairIterator.next();
            if (repair.getId().equals(part.getRepairId())) {
                repair.setPart(part);
                return;
            }
        }
    }

    public void updatePartRepairList(Long index, Part part) {
        for(Repair repair: REPAIR_LIST) {
            if(repair.getId()==part.getRepairId()) {
                for(int i = 0; i<repair.getNumberOfParts(); i++) {
                    if (index.equals(repair.getPart(i).getId())) {
                        repair.updatePart(i, part);
                        return;
                    }
                }
            }
        }
    }

    public boolean checkForId(Long id) {
        for(Repair repair: REPAIR_LIST) {
            if (repair.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

}
