package persistence;

import domain.User;
import domain.Vehicle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class UserDAO {
    private int id;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private ArrayList<Vehicle> listOfVehicles;
    private int afm;
    private String type;

    private List<User> USER_LIST = new ArrayList<>();

    private static UserDAO userDAO;
    private UserDAO() {}
    public static UserDAO getInstance() {
        if(userDAO==null) {
            userDAO = new UserDAO();
        }
        return userDAO;
    }

    public void addToList(User user) {
        USER_LIST.add(user);
    }

    public void deleteFromList(Long idToDelete) {
        Iterator<User> userIterator = USER_LIST.iterator();
        while(userIterator.hasNext()) {
            if(userIterator.next().getId().equals(idToDelete)) {
                userIterator.remove();
                return;
            }
        }
    }

    public void updateById(User userUpdated) {
        Iterator<User> userIterator = USER_LIST.iterator();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
            if(user.getId().equals(userUpdated.getId())) {
                int index = USER_LIST.indexOf(user);
                USER_LIST.set(index, userUpdated);
                return;
            }
        }
    }

    public User findByEmail(String email) throws NullPointerException {
        Iterator<User> userIterator = USER_LIST.iterator();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
                if (user.getEmail().equals(email)) {
                    return user;
                }
            }
        return null;
    }

    public List<User> findAll() {
        return USER_LIST;
    }

    public User login( String email, String password) throws NullPointerException {
        Iterator<User> userIterator = USER_LIST.iterator();
        while(userIterator.hasNext()) {
            User user = userIterator.next();
            if(user.getEmail().equals(email) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public List<Vehicle> findAllVehiclesByUser(Long userId) {
        Iterator<User> userIterator = USER_LIST.iterator();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
            if(user.getId().equals(userId)) {
                return user.getVehicleList();
            }
        }
        return null;
    }

    public void addVehicleToUser(Vehicle vehicle) {
        Iterator<User> userIterator = USER_LIST.iterator();
        while (userIterator.hasNext()) {
            User user = userIterator.next();
            if (user.getId().equals(vehicle.getUserID())) {
                user.addVehicleToList(vehicle);
                return;
            }
        }
    }

    public boolean checkForId(Long id) {
        for(User user: USER_LIST) {
            if(user.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public void updateVehicleUnUserList(Vehicle vehicle) {
        for(User user: USER_LIST) {
            if(user.getId()==vehicle.getUserID()) {
                for(int i = 0; i<user.getNumberOfVehicles(); i++) {
                    if (vehicle.getId().equals(user.getVehicle(i).getId())) {
                        user.updateVehicle(i,vehicle);
                        return;
                    }
                }
            }
        }
    }
}
