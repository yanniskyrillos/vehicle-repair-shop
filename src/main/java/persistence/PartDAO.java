package persistence;

import domain.Part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PartDAO {

    private List<Part> PART_LIST = new ArrayList<>();

    private static PartDAO partDAO;
    private PartDAO() {}
    public static PartDAO getInstance() {
        if (partDAO == null) {
            partDAO = new PartDAO();
        }
        return partDAO;
    }


    public void addPartToList(Part part) {
        PART_LIST.add(part);
    }

    public List<Part> findAllParts() {
        return PART_LIST;
    }

    public void removePartFromList(Part partToRemove) {
        PART_LIST.remove(partToRemove);
    }

    public void updatePartById(Long idToUpdate, Part partUpdated) {
        Iterator<Part> partIterator = PART_LIST.iterator();
        while (partIterator.hasNext()) {
            Part part = partIterator.next();
            if(part.getId().equals(idToUpdate)) {
                int index = PART_LIST.indexOf(part);
                PART_LIST.set(index, partUpdated);
                return;
            }
        }
    }

    public Part findPartById(Long idToFind) {
        for(Part part: PART_LIST) {
            if(part.getId().equals(idToFind)) {
                return part;
            }
        }
        return null;
    }
}
