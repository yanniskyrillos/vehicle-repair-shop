package persistence;

import domain.Vehicle;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VehicleDAO {

    private List<Vehicle> VEHICLE_LIST = new ArrayList<>();

    private static VehicleDAO vehicleDAO;
    private VehicleDAO() {}
    public static VehicleDAO getInstance() {
        if (vehicleDAO==null) {
            vehicleDAO = new VehicleDAO();
        }
        return vehicleDAO;
    }

    public void addToList(Vehicle vehicle) {
        VEHICLE_LIST.add(vehicle);
    }

    public List<Vehicle> findAll() {
        return VEHICLE_LIST;
    }

    public Vehicle findById(Long id) {
        for(Vehicle vehicle: VEHICLE_LIST) {
            if(vehicle.getId().equals(id)) {
                return vehicle;
            }
        }
        return null;
    }

    public void updateById(Vehicle vehicleUpdated) {
        Iterator<Vehicle> vehicleIterator = VEHICLE_LIST.iterator();
        while (vehicleIterator.hasNext()) {
            Vehicle vehicle = vehicleIterator.next();
            if(vehicle.getId().equals(vehicleUpdated.getId())) {
                int index = VEHICLE_LIST.indexOf(vehicle);
                VEHICLE_LIST.set(index, vehicleUpdated);
                return;
            }
        }
    }

    public void deleteById(Long id) {
        Iterator<Vehicle> vehicleIterator = VEHICLE_LIST.iterator();
        while(vehicleIterator.hasNext()) {
            Vehicle vehicle = vehicleIterator.next();
            if(vehicle.getId().equals(id)) {
                vehicleIterator.remove();
                return;
            }
        }
    }

    public boolean checkForId(Long id) {
        for(Vehicle vehicle:VEHICLE_LIST) {
            if(vehicle.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }
}
