package service;

import domain.Part;
import domain.Repair;
import domain.Vehicle;
import persistence.RepairDAO;

import java.util.List;

public class RepairService {

    public static RepairService repairService;
    private RepairService() {}
    public static RepairService getInstance() {
        if(repairService==null) {
            repairService = new RepairService();
        }
        return repairService;
    }

    RepairDAO repairDAO = RepairDAO.getInstance();

    static VehicleService vehicleService = VehicleService.getInstance();
    //VehicleService vehicleService;

    public boolean createRepair(Repair repair) {

        //VehicleService vehicleService = VehicleService.getInstance();
        //vehicleService = VehicleService.getInstance();

        boolean vehicleExists = vehicleService.checkForId(repair.getVehicleId());
        if(vehicleExists) {
            repairDAO.addRepairToList(repair);
            return true;
        }
        else {
            return false;
        }
    }
    public List<Repair> findAllRepairs() {
        return repairDAO.findAllRepairs();
    }
    public Repair findRepairById(Long id) {
        return repairDAO.findRepairById(id);
    }
    public void updateRepairById(Long id, Repair repair) {
        repairDAO.updateRepairById(id, repair);
    }
    public void deleteRepairById(Long id) {
        repairDAO.deleteRepairById(id);
    }
    public double calculateTotalRepairCost(Long id) {
        return repairDAO.calculateCostOfRepairAndParts(id);
    }

    public void removePartFromList(Part partToRemove) {
        repairDAO.removePart(partToRemove);
    }

    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        return repairDAO.findAllRepairsByVehicle(vehicleId);
    }

    public void addPartToRepair(Part part) {
        repairDAO.addPartToRepair(part);
    }

    public void updatePartRepairList(Long index, Part part) {
        repairDAO.updatePartRepairList(index, part);
    }

    public boolean checkForId(Long id) {
        return repairDAO.checkForId(id);
    }
}
