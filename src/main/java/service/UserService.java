package service;

import domain.Repair;
import domain.User;
import domain.Vehicle;
import persistence.UserDAO;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private static UserService userService;
    private UserService() {}
    public static UserService getInstance() {
        if(userService==null) {
            userService = new UserService();
        }
        return userService;
    }

    UserDAO userDAO = UserDAO.getInstance();
    RepairService repairService =RepairService.getInstance();

    public boolean create(User user) {
        boolean userIdAlreadyInUse = userDAO.checkForId(user.getId());
        if (userIdAlreadyInUse) {
            return false;
        }
        userDAO.addToList(user);
        return true;
    }

    public void delete(Long idToDelete) {
        userDAO.deleteFromList(idToDelete);
    }

    public void updateById(User user) {
        userDAO.updateById(user);
    }

    public User findByEmail(String email) throws NullPointerException {
        return userDAO.findByEmail(email);
    }

    public List<User> findAll() {
        return userDAO.findAll();
    }

    public User login(String email, String password) throws NullPointerException {
        return userDAO.login(email, password);
    }

    public List<Vehicle> findAllVehiclesByUeer(Long userId) {
        return userDAO.findAllVehiclesByUser(userId);
    }

    public List<Repair> findAllRepairsOfAllVehiclesByUser(Long userId) {
        List<Repair> repairsOfAllVehiclesOfUser = new ArrayList<>();
        List<Vehicle> allVehiclesByUser = userDAO.findAllVehiclesByUser(userId);
        for(Vehicle vehicle: allVehiclesByUser) {
            repairsOfAllVehiclesOfUser.addAll(repairService.findAllRepairsByVehicle(vehicle.getId()));
        }
        return repairsOfAllVehiclesOfUser;
    }

    public void addVehicleToUser(Vehicle vehicle) {
        userDAO.addVehicleToUser(vehicle);
    }

    public boolean checkForId(Long id) {
        return userDAO.checkForId(id);
    }

    public void updateVehicleInUserList(Vehicle vehicle) {
        userDAO.updateVehicleUnUserList(vehicle);
    }
}
