package service;

import domain.Part;
import persistence.PartDAO;

import java.util.List;

public class PartService {

    private static PartService partService;
    private PartService() {}
    public static PartService getInstance() {
        if(partService==null) {
            partService = new PartService();
        }
        return partService;
    }

    RepairService repairService = RepairService.getInstance();
    PartDAO partDAO = PartDAO.getInstance();

    public boolean createPart( Part part) {
        boolean repairExists = repairService.checkForId(part.getRepairId());
        if (repairExists) {
            partDAO.addPartToList(part);
            repairService.addPartToRepair(part);
            return true;
        }
        else {
            return false;
        }
    }

    public List<Part> findAllParts() {
        return partDAO.findAllParts();
    }
    public void deletePart(Part partToRemove) {
        partDAO.removePartFromList(partToRemove);
        repairService.removePartFromList(partToRemove);
    }
    public void updatePartById(Long idToUpdate, Part partUpdated) {
        partDAO.updatePartById(idToUpdate, partUpdated);
        repairService.updatePartRepairList(idToUpdate, partUpdated);
    }
    public Part findPartById(Long idToFind) {
        return partDAO.findPartById(idToFind);
    }
}
