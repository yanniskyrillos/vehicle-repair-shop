package service;

import domain.Repair;
import domain.Vehicle;
import persistence.VehicleDAO;

import java.util.List;

public class VehicleService {

    private static VehicleService vehicleService;
    private VehicleService() {}
    public static VehicleService getInstance() {
        if(vehicleService==null) {
            vehicleService = new VehicleService();
        }
        return vehicleService;
    }

    VehicleDAO vehicleDAO = VehicleDAO.getInstance();
    UserService userService = UserService.getInstance();
    RepairService repairService = RepairService.getInstance();


    public boolean create(Vehicle vehicle) {
        boolean userExists = userService.checkForId(vehicle.getUserID());
        if (userExists) {
            vehicleDAO.addToList(vehicle);
            userService.addVehicleToUser(vehicle);
            return true;
        }
        return false;
    }
    public List<Vehicle> findAll() {
        return vehicleDAO.findAll();
    }
    public Vehicle findById(Long id) {
        return vehicleDAO.findById(id);
    }
    public void updateById(Vehicle vehicle) {
        vehicleDAO.updateById(vehicle);
        userService.updateVehicleInUserList(vehicle);
    }
    public void deleteById(Long id) {
        vehicleDAO.deleteById(id);
    }
    public List<Repair> findAllRepairsByVehicle(Long vehicleId) {
        return repairService.findAllRepairsByVehicle(vehicleId);
    }

    public boolean checkForId(Long id) {
        return vehicleDAO.checkForId(id);
    }
}
