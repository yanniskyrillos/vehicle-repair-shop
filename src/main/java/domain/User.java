package domain;


import java.util.ArrayList;
import java.util.List;

public class User {
    private Long id;
    //private Long newID;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private List<Vehicle> listOfVehicles = new ArrayList<>();
    private int afm;
    private String type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAfm() {
        return afm;
    }

    public void setAfm(int afm) {
        this.afm = afm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Vehicle> getVehicleList() {
        return this.listOfVehicles;
    }

    public void addVehicleToList(Vehicle vehicle) {
        this.listOfVehicles.add(vehicle);
    }

    public void updateVehicle(int index, Vehicle vehicle) {
        this.listOfVehicles.set(index, vehicle);
    }

    public int getNumberOfVehicles() {
        return listOfVehicles.size();
    }

    public Vehicle getVehicle(int vehicleIndexInList) {
        return listOfVehicles.get(vehicleIndexInList);
    }

    public User(Long id, String email, String password, String firstName, String lastName, String address,
                int afm, String type) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.afm = afm;
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "lastName='" + lastName + '\'' +
                '}';
    }
}
