package domain;

import util.RepairStatus;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;



public class Repair {
    private Long id;
    LocalDate date;
    //private String status;
    private RepairStatus status;
    private Long vehicleId;
    private double cost;
    private List<Part> listOfParts = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RepairStatus getStatus() {
        return status;
    }

    public void setStatus(RepairStatus status) {
        this.status = status;
    }

    public Long getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public Part getPart(int partIndexInList) {
        return listOfParts.get(partIndexInList);
    }

    public void setPart(Part part) {
        listOfParts.add(part);
    }

    public int getNumberOfParts() {
        return listOfParts.size();
    }

    public void removePart(Part part) {
        listOfParts.remove(part);
    }

    public void updatePart(int index, Part part) {
        listOfParts.set(index, part);
    }

    @Override
    public String toString() {
        return "Repair{" +
                "date='" + date + '\'' +
                ", status='" + status + '\'' +
                ", vehicleId=" + vehicleId +
                ", cost=" + cost +
                '}';
    }

    public Repair(Long id, RepairStatus status, Long vehicleId, double cost, int year, int month, int day) {
        this.id = id;
        this.status = status;
        this.vehicleId = vehicleId;
        this.cost = cost;
        this.date = LocalDate.of(year, month, day);
    }
}
