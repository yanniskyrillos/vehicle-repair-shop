package util;

public enum RepairStatus {
        PENDING,
        COMPLETED,
        CANCELLED;
}
